// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "DrawDebugHelpers.h"

#include "LorenzActor.generated.h"

UCLASS()
class LORENZSYSTEM_API ALorenzActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ALorenzActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	float X;

	float Y;

	float Z;

	float PointsSize;

	float WorldScale;

	UFUNCTION(BlueprintCallable, Category = "Lorenz")
	void CreateLorenzSystem(float a, float b, float c, float DeltaTime, FVector &out_Result); // a - omega | b - beta | c - p

	void InitializeCoordinates();

	UFUNCTION(BlueprintCallable, Category = "LorenzTest")
	void DrawPoints(FColor PointColor);

	UFUNCTION(BlueprintCallable, Category = "Aizawa")
	void CreateAizawaAttractor(float a, float b, float c, float d, float e, float f, float DeltaTime, FVector &out_Result);

	UFUNCTION(BlueprintCallable, Category = "De Jong")
	void CreateDeJongAttractor(float a, float b, float c, float d, float DeltaTime, FVector &out_Result);
	
	UPROPERTY(BlueprintReadOnly)
	TArray<FVector> Points;

	TArray<FColor> Colors;


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};
