// Fill out your copyright notice in the Description page of Project Settings.

#include "LorenzActor.h"


// Sets default values
ALorenzActor::ALorenzActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	WorldScale = 50.f;
}

// Called when the game starts or when spawned
void ALorenzActor::BeginPlay()
{
	Super::BeginPlay();
	InitializeCoordinates();
	
}

void ALorenzActor::CreateLorenzSystem(float a, float b, float c, float DeltaTime, FVector &out_Result) // a - sigma | b - beta | c - p
{
	DeltaTime = (DeltaTime / 0.0015) * 0.001;
	float dx = (a * (Y - X)) * DeltaTime;
	float dy = (X * (c - Z) - Y) * DeltaTime;
	float dz = (X * Y - b * Z) * DeltaTime;
	
	X = X + dx;
	Y = Y + dy;
	Z = Z + dz;

	out_Result = FVector(X * WorldScale, Y * WorldScale, Z * WorldScale);
	Points.Add(FVector(out_Result));
}



void ALorenzActor::InitializeCoordinates()
{
	X = 1;
	Y = 1;
	Z = 1;

	PointsSize = 1.5f;
}

void ALorenzActor::DrawPoints(FColor PointColor)
{
	UWorld* MyWorld = GetWorld();
	if (MyWorld)
	{
		for (int32 i = 0; i < Points.Num(); i++)
		{
			DrawDebugPoint(MyWorld, Points[i], PointsSize, PointColor, false);		
		}
	}
	else 
	{
		return;
	}

}

void ALorenzActor::CreateAizawaAttractor(float a, float b, float c, float d, float e, float f, float DeltaTime, FVector &out_Result)
{
	DeltaTime = (DeltaTime / 0.0015) * 0.002;
	float dx = ((Z - b) * X - d * Y) * DeltaTime;
	float dy = (d * X + (Z - b) * Y) * DeltaTime;
	float dz = (c + a * Z - FMath::Pow(Z, 3) / 3 - FMath::Pow(X, 2) + f * Z * FMath::Pow(X, 3)) * DeltaTime;
	
	X = X + dx;
	Y = Y + dy;
	Z = Z + dz;

	out_Result = FVector(X * WorldScale, Y * WorldScale, Z * WorldScale);
	Points.Add(FVector(out_Result));
}

void ALorenzActor::CreateDeJongAttractor(float a, float b, float c, float d, float DeltaTime, FVector &out_Result)
{
	DeltaTime = (DeltaTime / 0.0015) * 0.002;
	float x = FMath::Sin(a * (Y * DeltaTime)) - FMath::Cos(b * (X * DeltaTime));
	float y = FMath::Sin(c * (X * DeltaTime)) - FMath::Cos(d * (Y * DeltaTime));

	X = X + x;
	Y = Y + y;
	Z = 0;

	out_Result = FVector(X, Y, Z) * WorldScale;
	Points.Add(FVector(out_Result));
}

// Called every frame
void ALorenzActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

